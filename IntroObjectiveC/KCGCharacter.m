//
//  KCGCharacter.m
//  IntroObjectiveC
//
//  Created by Fernando Rodríguez Romero on 11/01/16.
//  Copyright © 2016 keepcoding.io. All rights reserved.
//

#import "KCGCharacter.h"

@implementation KCGCharacter

#pragma mark - Class Methods
+(instancetype) characterWithName:(NSString*) name
                              age:(int) age{
    return [[self alloc] initWithName:name
                                  age:age];
    
}

+(instancetype) characterWithName:(NSString*) name{
    
    return [[self alloc] initWithName:name];
}

#pragma mark - Initialization
// designated
-(id) initWithName:(NSString *)name
               age:(int)age{
    

    if (self = [super init]) {
        _name = name;
        _age  = age;
    }
    
    return  self;
}

-(id) initWithName:(NSString *)name{
    
    return [self initWithName:name
                          age:0];
}


-(BOOL)isNameSake:(KCGCharacter *) other{
    
    if( [self.name isEqualToString:other.name]){
        return YES;
    }else{
        return NO;
    }
    
}

















@end
