//
//  KCGCharacter.h
//  IntroObjectiveC
//
//  Created by Fernando Rodríguez Romero on 11/01/16.
//  Copyright © 2016 keepcoding.io. All rights reserved.
//

@import Foundation;

@interface KCGCharacter : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic) int age;

+(instancetype) characterWithName:(NSString*) name
                              age:(int) age;

+(instancetype) characterWithName:(NSString*) name;

-(id) initWithName:(NSString*) name
               age:(int) age;

-(id) initWithName:(NSString *)name;

-(BOOL)isNameSake:(KCGCharacter *) other;


@end
